import firebase from 'firebase/app';
import 'firebase/firestore'
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyCT0h3j7ELqUd8o6a1Tz92w2LsEX_N3LpA",
    authDomain: "rjs-ecom.firebaseapp.com",
    databaseURL: "https://rjs-ecom.firebaseio.com",
    projectId: "rjs-ecom",
    storageBucket: "rjs-ecom.appspot.com",
    messagingSenderId: "637347372358",
    appId: "1:637347372358:web:c80633c1d3369459b61947",
    measurementId: "G-JSLQ63W75B"
  };

export const createUserProfileDocument = async(userAuth, additionalData) => {
    if(!userAuth) return;

    const userRef = firestore.doc(`users/${userAuth.uid}`);
    const snapShot = await userRef.get()
    if(!snapShot.exists) {
        const { displayName, email } = userAuth;
        const createdAt = new Date();

        try {
            await userRef.set({
                displayName,
                email,
                createdAt,
                ...additionalData  
            })
        } catch(e) {
            console.log('Error Creating User', e.messsage)
        }
    }
    return userRef;
}

firebase.initializeApp(config);


export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({prompt: 'select_account'});
export const signInWithGoogle = () => auth.signInWithPopup(provider);
export default firebase;
